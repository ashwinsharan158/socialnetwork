(self["webpackChunkToDoApp3"] = self["webpackChunkToDoApp3"] || []).push([["src_app_swiper_swiper_module_ts"],{

/***/ 1382:
/*!*************************************************!*\
  !*** ./src/app/swiper/swiper-routing.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SwiperPageRoutingModule": () => (/* binding */ SwiperPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _swiper_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./swiper.page */ 5338);




const routes = [
    {
        path: '',
        component: _swiper_page__WEBPACK_IMPORTED_MODULE_0__.SwiperPage
    }
];
let SwiperPageRoutingModule = class SwiperPageRoutingModule {
};
SwiperPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SwiperPageRoutingModule);



/***/ }),

/***/ 2146:
/*!*****************************************!*\
  !*** ./src/app/swiper/swiper.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SwiperPageModule": () => (/* binding */ SwiperPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _swiper_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./swiper-routing.module */ 1382);
/* harmony import */ var _swiper_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./swiper.page */ 5338);







let SwiperPageModule = class SwiperPageModule {
};
SwiperPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _swiper_routing_module__WEBPACK_IMPORTED_MODULE_0__.SwiperPageRoutingModule
        ],
        declarations: [_swiper_page__WEBPACK_IMPORTED_MODULE_1__.SwiperPage]
    })
], SwiperPageModule);



/***/ }),

/***/ 5338:
/*!***************************************!*\
  !*** ./src/app/swiper/swiper.page.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SwiperPage": () => (/* binding */ SwiperPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_swiper_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./swiper.page.html */ 7796);
/* harmony import */ var _swiper_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./swiper.page.scss */ 7569);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let SwiperPage = class SwiperPage {
    constructor() { }
    ngOnInit() {
    }
};
SwiperPage.ctorParameters = () => [];
SwiperPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-swiper',
        template: _raw_loader_swiper_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_swiper_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SwiperPage);



/***/ }),

/***/ 7569:
/*!*****************************************!*\
  !*** ./src/app/swiper/swiper.page.scss ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".swiper-slide {\n  display: block;\n}\n\n.swiper-slide h2 {\n  margin-top: 2.8rem;\n}\n\n.swiper-slide img {\n  max-height: 50%;\n  max-width: 80%;\n  margin: 60px 0 40px;\n  pointer-events: none;\n}\n\nb {\n  font-weight: 500;\n}\n\np {\n  padding: 0 40px;\n  font-size: 14px;\n  line-height: 1.5;\n  color: var(--ion-color-step-600, #60646b);\n}\n\np b {\n  color: var(--ion-text-color, #000000);\n}\n\nion-slides {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN3aXBlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0FBQ0o7O0FBRUU7RUFDRSxrQkFBQTtBQUNKOztBQUVFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtBQUNKOztBQUVFO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBRUU7RUFDRSxxQ0FBQTtBQUNKOztBQUVFO0VBQ0UsWUFBQTtBQUNKIiwiZmlsZSI6InN3aXBlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3dpcGVyLXNsaWRlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnN3aXBlci1zbGlkZSBoMiB7XHJcbiAgICBtYXJnaW4tdG9wOiAyLjhyZW07XHJcbiAgfVxyXG5cclxuICAuc3dpcGVyLXNsaWRlIGltZyB7XHJcbiAgICBtYXgtaGVpZ2h0OiA1MCU7XHJcbiAgICBtYXgtd2lkdGg6IDgwJTtcclxuICAgIG1hcmdpbjogNjBweCAwIDQwcHg7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICB9XHJcblxyXG4gIGIge1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB9XHJcblxyXG4gIHAge1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc3RlcC02MDAsICM2MDY0NmIpO1xyXG4gIH1cclxuXHJcbiAgcCBiIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvciwgIzAwMDAwMCk7XHJcbiAgfVxyXG5cclxuICBpb24tc2xpZGVzIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9Il19 */");

/***/ }),

/***/ 7796:
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/swiper/swiper.page.html ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\r\n  <ion-app>\r\n    <ion-content fullscreen class=\"ion-padding\" scroll-y=\"false\">\r\n      <ion-slides>\r\n\r\n        <ion-slide>\r\n          <div class=\"slide\">\r\n            <img src=\"https://media.istockphoto.com/vectors/vinyl-record-template-vector-id481475560?k=6&m=481475560&s=612x612&w=0&h=6v5l7rebyR2T8zXxlssd9jBzVfE0Vaw2qDWID-IMdzI=\"/>\r\n            <h2>Ashwin</h2>\r\n            <p><b>From Delhi, 26, Male</b> <br>Tap to hear his question <br> <i>\"The distance between insanity and genius is measured only by success\"</i></p>\r\n          </div>\r\n        </ion-slide>\r\n\r\n        <ion-slide>\r\n          <img src=\"https://media.istockphoto.com/photos/vinyl-record-album-picture-id134119582?k=6&m=134119582&s=612x612&w=0&h=j77_D1ZFAi5ZVfa9pHp8yxY1v7aetEbjTktISvxXAAI=\"/>\r\n          <h2>Lydia</h2>\r\n          <p><b>Ionic Framework</b> is an open source SDK that enables developers to build high quality mobile apps with web technologies like HTML, CSS, and JavaScript.</p>\r\n        </ion-slide>\r\n\r\n        <ion-slide>\r\n          <img src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqsBkC5zqxTmZm81TbpvjneDRQgBSZx0oLrNZwtzRrKVThgUEUVYvrFIerVQrqutU_RT0&usqp=CAU\"/>\r\n          <h2>Dave</h2>\r\n          <p><b>Appflow</b> is a powerful set of services and features built on top of Ionic Framework that brings a totally new level of app development agility to mobile dev teams.</p>\r\n        </ion-slide>\r\n\r\n        <ion-slide>\r\n          <img src=\"https://www.techgenesis.net/wp-content/uploads/2021/05/Music-Maker-JAM-for-Windows.png\"/>\r\n          <h2>Ready for more?</h2>\r\n          <ion-button fill=\"clear\">load more! <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon></ion-button>\r\n        </ion-slide>\r\n\r\n      </ion-slides>\r\n    </ion-content>\r\n  </ion-app>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_swiper_swiper_module_ts.js.map