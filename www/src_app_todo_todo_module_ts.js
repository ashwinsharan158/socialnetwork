(self["webpackChunkToDoApp3"] = self["webpackChunkToDoApp3"] || []).push([["src_app_todo_todo_module_ts"],{

/***/ 7482:
/*!*********************************************!*\
  !*** ./src/app/todo/todo-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TodoPageRoutingModule": () => (/* binding */ TodoPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _todo_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todo.page */ 2346);




const routes = [
    {
        path: '',
        component: _todo_page__WEBPACK_IMPORTED_MODULE_0__.TodoPage
    }
];
let TodoPageRoutingModule = class TodoPageRoutingModule {
};
TodoPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], TodoPageRoutingModule);



/***/ }),

/***/ 8751:
/*!*************************************!*\
  !*** ./src/app/todo/todo.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TodoPageModule": () => (/* binding */ TodoPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _todo_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todo-routing.module */ 7482);
/* harmony import */ var _todo_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todo.page */ 2346);







let TodoPageModule = class TodoPageModule {
};
TodoPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _todo_routing_module__WEBPACK_IMPORTED_MODULE_0__.TodoPageRoutingModule
        ],
        declarations: [_todo_page__WEBPACK_IMPORTED_MODULE_1__.TodoPage]
    })
], TodoPageModule);



/***/ }),

/***/ 2346:
/*!***********************************!*\
  !*** ./src/app/todo/todo.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TodoPage": () => (/* binding */ TodoPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_todo_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./todo.page.html */ 4576);
/* harmony import */ var _todo_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todo.page.scss */ 3303);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ 6797);






let TodoPage = class TodoPage {
    constructor(toastCtrl, navCtrl) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.todos = [];
        this.userId = firebase__WEBPACK_IMPORTED_MODULE_2__.default.auth().currentUser.uid;
        this.getToDos();
    }
    ngOnInit() {
    }
    getToDos() {
        firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore().collection("User_data")
            .where("owner", "==", this.userId)
            .where("status", "==", "incomplete")
            .onSnapshot((querySnapshot) => {
            this.todos = querySnapshot.docs;
        });
    }
    getDate(timestamp) {
        let date = timestamp.toDate();
        return date.toLocaleDateString();
    }
    gotoAddToDo() {
        this.navCtrl.navigateForward(['/add-todo']);
    }
    markCompleted(document) {
        firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore().collection("todos").doc(document.id).set({
            "status": "completed"
        }, {
            merge: true
        }).then(() => {
            this.toastCtrl.create({
                message: "Todo item has been marked completed!",
                duration: 2000
            }).then((toast) => {
                toast.present();
            });
        });
    }
    logout() {
        firebase__WEBPACK_IMPORTED_MODULE_2__.default.auth().signOut().then(() => {
            this.navCtrl.navigateRoot("/login");
        }).catch((err) => {
            console.log(err);
        });
    }
};
TodoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.NavController }
];
TodoPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-todo',
        template: _raw_loader_todo_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_todo_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TodoPage);



/***/ }),

/***/ 3303:
/*!*************************************!*\
  !*** ./src/app/todo/todo.page.scss ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0b2RvLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 4576:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/todo/todo.page.html ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Your Todo</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" slot=\"icon-only\" (click)=\"logout()\">\r\n        <ion-icon name=\"log-out\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-list>\r\n    <ion-item-sliding *ngFor=\"let todo of todos\">\r\n      <ion-item>\r\n        <ion-label>\r\n          <h2> {{ todo.data().title}}</h2>\r\n          <h3> {{ todo.data().description }}</h3>\r\n          <p> {{ todo.data().last_date }}</p>\r\n        </ion-label>\r\n      </ion-item>\r\n      \r\n      <ion-item-options side=\"end\">\r\n        <ion-item-option color=\"secondary\" (click)=\"markCompleted(todo)\">\r\n          <ion-icon name=\"checkmark\"></ion-icon>\r\n        </ion-item-option>\r\n      </ion-item-options>\r\n    </ion-item-sliding>\r\n  </ion-list>\r\n\r\n  <ion-fab horizontal=\"end\" vertical=\"bottom\" slot=\"fixed\" (click)=\"gotoAddToDo()\">\r\n    <ion-fab-button color=\"primary\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_todo_todo_module_ts.js.map