(self["webpackChunkToDoApp3"] = self["webpackChunkToDoApp3"] || []).push([["src_app_signup_signup_module_ts"],{

/***/ 159:
/*!*************************************************!*\
  !*** ./src/app/signup/signup-routing.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPageRoutingModule": () => (/* binding */ SignupPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup.page */ 771);




const routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_0__.SignupPage
    }
];
let SignupPageRoutingModule = class SignupPageRoutingModule {
};
SignupPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SignupPageRoutingModule);



/***/ }),

/***/ 7648:
/*!*****************************************!*\
  !*** ./src/app/signup/signup.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPageModule": () => (/* binding */ SignupPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup-routing.module */ 159);
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup.page */ 771);







let SignupPageModule = class SignupPageModule {
};
SignupPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _signup_routing_module__WEBPACK_IMPORTED_MODULE_0__.SignupPageRoutingModule
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_1__.SignupPage]
    })
], SignupPageModule);



/***/ }),

/***/ 771:
/*!***************************************!*\
  !*** ./src/app/signup/signup.page.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPage": () => (/* binding */ SignupPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./signup.page.html */ 1355);
/* harmony import */ var _signup_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup.page.scss */ 4194);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ 6797);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 476);






let SignupPage = class SignupPage {
    constructor(toastCtrl, navCtrl) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
    }
    RadioChangeEvent(event) {
        this.sex = event.detail.value;
    }
    RadioChangeEventTwo(event) {
        this.want_sex = event.detail.value;
    }
    ngOnInit() {
    }
    signup() {
        firebase__WEBPACK_IMPORTED_MODULE_2__.default.auth().createUserWithEmailAndPassword(this.email, this.password).then((data) => {
            console.log(data);
            this.UserId = firebase__WEBPACK_IMPORTED_MODULE_2__.default.auth().currentUser.uid;
            firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore().collection("User_data").add({
                f_name: this.f_name,
                location: this.location,
                sex: this.sex,
                want_sex: this.want_sex,
                age: this.age,
                owner: this.UserId,
                email: this.email,
                create: firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore.FieldValue.serverTimestamp()
            }).then((docRef) => {
                this.toastCtrl.create({
                    message: "User info has been added",
                    duration: 2000
                }).then((toast) => {
                    toast.present();
                    this.navCtrl.navigateForward(['/profile']);
                });
            }).catch((err) => {
                this.toastCtrl.create({
                    message: err.message,
                    duration: 2000
                }).then((toast) => {
                    toast.present();
                });
            });
        }).catch((err) => {
            this.toastCtrl.create({
                message: err.message,
                duration: 3000
            }).then((toast) => {
                toast.present();
            });
        });
    }
    gotoLogin() {
        this.navCtrl.back();
    }
};
SignupPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.NavController }
];
SignupPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-signup',
        template: _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_signup_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SignupPage);



/***/ }),

/***/ 4194:
/*!*****************************************!*\
  !*** ./src/app/signup/signup.page.scss ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".vertical-space {\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZ251cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6InNpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudmVydGljYWwtc3BhY2V7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59Il19 */");

/***/ }),

/***/ 1355:
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header >\r\n  <ion-toolbar color =\"primary\">\r\n    <ion-back-button color=\"light\"></ion-back-button>\r\n    <ion-title>\r\n      Come On In!!\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content fullscreen>\r\n  <ion-list lines=\"full\" class=\"ion-no-margin\">\r\n    <ion-list-header lines=\"full\">\r\n      <ion-label>\r\n       <b>What do we call you?</b> \r\n      </ion-label>\r\n    </ion-list-header>\r\n    <ion-item>\r\n      <ion-label>First Name</ion-label>\r\n      <ion-input placeholder=\"ScoopDogg\" [(ngModel)]=\"f_name\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Email</ion-label>\r\n      <ion-input type=\"email\" placeholder=\"WhiteWa@email.com\" [(ngModel)]=\"email\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"fixed\">Location</ion-label>\r\n      <ion-input placeholder=\"Narnia\" [(ngModel)]=\"location\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"fixed\">Age </ion-label>\r\n      <ion-input placeholder=\"31\" [(ngModel)]=\"age\"></ion-input>\r\n    </ion-item>\r\n      <ion-radio-group lines=\"full\" (ionChange)=\"RadioChangeEvent($event)\">\r\n        <ion-list-header>\r\n          <ion-label>Sexual Orentation</ion-label>\r\n        </ion-list-header> \r\n      <ion-item>\r\n        <ion-label>Man</ion-label>\r\n        <ion-radio slot=\"start\" value=\"Man\" ></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>Women</ion-label>\r\n        <ion-radio slot=\"start\" value=\"Women\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>Triggered</ion-label>\r\n        <ion-radio slot=\"start\" value=\"Triggered\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>Bi</ion-label>\r\n        <ion-radio slot=\"start\" value=\"Bi\"></ion-radio>\r\n      </ion-item>\r\n    </ion-radio-group>\r\n    <ion-radio-group lines=\"full\" (ionChange)=\"RadioChangeEventTwo($event)\">\r\n      <ion-list-header>\r\n        <ion-label>Sexual Preferences</ion-label>\r\n      </ion-list-header> \r\n    <ion-item>\r\n      <ion-label>Man</ion-label>\r\n      <ion-radio slot=\"start\" value=\"Man\" ></ion-radio>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label>Women</ion-label>\r\n      <ion-radio slot=\"start\" value=\"Women\"></ion-radio>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label>Prefer not to say</ion-label>\r\n      <ion-radio slot=\"start\" value=\"Prefer not to say\"></ion-radio>\r\n    </ion-item>\r\n  </ion-radio-group>\r\n    <ion-item>\r\n      <ion-label>Password</ion-label>\r\n      <ion-input placeholder=\"\" type=\"password\" [(ngModel)]=\"password\"></ion-input>\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-button expand=\"full\" fill=\"outline\" color =\"secondry\" shape=\"round\" (click)=\"signup()\">SUBMIT</ion-button>\r\n  <ion-button fill=\"clear\" color =\"danger\" (click)=\"gotoLogin()\">Already have an Account? login here</ion-button>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_signup_signup_module_ts.js.map