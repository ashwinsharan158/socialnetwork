(self["webpackChunkToDoApp3"] = self["webpackChunkToDoApp3"] || []).push([["src_app_add-todo_add-todo_module_ts"],{

/***/ 6925:
/*!*****************************************************!*\
  !*** ./src/app/add-todo/add-todo-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddTodoPageRoutingModule": () => (/* binding */ AddTodoPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _add_todo_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-todo.page */ 1637);




const routes = [
    {
        path: '',
        component: _add_todo_page__WEBPACK_IMPORTED_MODULE_0__.AddTodoPage
    }
];
let AddTodoPageRoutingModule = class AddTodoPageRoutingModule {
};
AddTodoPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AddTodoPageRoutingModule);



/***/ }),

/***/ 5200:
/*!*********************************************!*\
  !*** ./src/app/add-todo/add-todo.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddTodoPageModule": () => (/* binding */ AddTodoPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _add_todo_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-todo-routing.module */ 6925);
/* harmony import */ var _add_todo_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-todo.page */ 1637);







let AddTodoPageModule = class AddTodoPageModule {
};
AddTodoPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _add_todo_routing_module__WEBPACK_IMPORTED_MODULE_0__.AddTodoPageRoutingModule
        ],
        declarations: [_add_todo_page__WEBPACK_IMPORTED_MODULE_1__.AddTodoPage]
    })
], AddTodoPageModule);



/***/ }),

/***/ 1637:
/*!*******************************************!*\
  !*** ./src/app/add-todo/add-todo.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddTodoPage": () => (/* binding */ AddTodoPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_add_todo_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./add-todo.page.html */ 8435);
/* harmony import */ var _add_todo_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-todo.page.scss */ 2338);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ 6797);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 476);






let AddTodoPage = class AddTodoPage {
    constructor(toastCtrl, navCtrl) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.todo_owner = firebase__WEBPACK_IMPORTED_MODULE_2__.default.auth().currentUser.uid;
    }
    ngOnInit() {
    }
    addTodo() {
        firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore().collection("todos").add({
            title: this.todo_title,
            description: this.todo_des,
            last_date: new Date(this.todo_date),
            owner: this.todo_owner,
            status: "incomplete",
            create: firebase__WEBPACK_IMPORTED_MODULE_2__.default.firestore.FieldValue.serverTimestamp()
        }).then((docRef) => {
            this.toastCtrl.create({
                message: "ToDp has been added!",
                duration: 2000
            }).then((toast) => {
                toast.present();
                this.navCtrl.back();
            });
        }).catch((err) => {
            this.toastCtrl.create({
                message: err.message,
                duration: 2000
            }).then((toast) => {
                toast.present();
            });
        });
    }
};
AddTodoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.NavController }
];
AddTodoPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-add-todo',
        template: _raw_loader_add_todo_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_add_todo_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AddTodoPage);



/***/ }),

/***/ 2338:
/*!*********************************************!*\
  !*** ./src/app/add-todo/add-todo.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGQtdG9kby5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 8435:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/add-todo/add-todo.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>add-todo</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-list>\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">ToDo Title</ion-label>\r\n      <ion-input type=\"text\" [(ngModel)]=\"todo_title\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">ToDo Description</ion-label>\r\n      <ion-input type=\"text\" [(ngModel)]=\"todo_des\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">ToDo Last Date</ion-label>\r\n      <ion-datetime min=\"2021\" display-format=\"mm/DD/YYYY\" [(ngModel)]=\"todo_date\"></ion-datetime>\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-button expand=\"full\" fill=\"outline\" color =\"secondry\" shape=\"round\" (click)=\"addTodo()\">Add ToDO</ion-button>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_add-todo_add-todo_module_ts.js.map