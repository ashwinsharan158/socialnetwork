import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import firebase from 'firebase';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
})
export class TodoPage implements OnInit {

  userId: string;
  todos:any[] = [];

  constructor(private toastCtrl: ToastController, private navCtrl: NavController) {
    this.userId = firebase.auth().currentUser.uid;
    this.getToDos();
   }

  ngOnInit() {
  }

  getToDos(){

    firebase.firestore().collection("User_data")
    .where("owner","==", this.userId)
    .where("status", "==", "incomplete")
    .onSnapshot((querySnapshot) => {
      this.todos = querySnapshot.docs;
    });
  }

  getDate(timestamp: firebase.firestore.Timestamp){
    let date = timestamp.toDate();
    return date.toLocaleDateString();
  }

  gotoAddToDo(){
    this.navCtrl.navigateForward(['/add-todo']);
  }

  markCompleted(document: firebase.firestore.QueryDocumentSnapshot)
  {
    firebase.firestore().collection("todos").doc(document.id).set({
     "status":"completed" 
    },{
      merge: true
    }).then(() => {
        this.toastCtrl.create({
          message: "Todo item has been marked completed!",
          duration: 2000
        }).then((toast) => {
          toast.present();
        })
    })
  }

  logout(){
    firebase.auth().signOut().then(() => {
      this.navCtrl.navigateRoot("/login");
    }).catch((err) => {
      console.log(err)
    })
  }

}
