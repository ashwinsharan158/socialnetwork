import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import firebase from 'firebase';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  userId: string;
  U_data:any[] = [];

  constructor(private toastCtrl: ToastController, private navCtrl: NavController) {
    this.userId = firebase.auth().currentUser.uid;
    this.getUserData();
   }

   getUserData(){
    firebase.firestore().collection("User_data").where("owner","==",this.userId).get().then((query) => {
      console.log("User Data: ")
      console.log(query.docs);
      this.U_data = query.docs;
    }).catch((err) => {
      console.log(err)
    })
  }

  logout(){
    firebase.auth().signOut().then(() => {
      this.navCtrl.navigateRoot("/login");
    }).catch((err) => {
      console.log(err)
    })
  }

  gotoChat(){
    this.navCtrl.navigateRoot("/chat");
  }
  gotoSwiper(){
    this.navCtrl.navigateRoot("/swiper");
  }
  ngOnInit() {
  }

}
