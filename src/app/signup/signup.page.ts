import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  f_name: string;
  location: string;
  sex: string;
  age: string;
  preferred_sex: string;
  preferred_location: string;
  email: string;
  password: string;
  UserId: string;

  RadioChangeEvent(event){
    this.sex = event.detail.value;
  }

  RadioChangeEventTwo(event){
    this.preferred_sex = event.detail.value;
  }

  RadioChangeEventThree(event){
    this.preferred_location = event.detail.value;
  }
  constructor(private toastCtrl: ToastController, private navCtrl: NavController) { }

  ngOnInit() {
  }

  signup() {
    let location: string;
    if(!this.preferred_location.localeCompare("Anywhere"))
    {
        location = "Anywhere";
    }
    else
    {
        location = this.location;
    }
    firebase.auth().createUserWithEmailAndPassword(this.email,
    this.password).then((data) => {
      console.log(data);
      this.UserId = firebase.auth().currentUser.uid
      firebase.firestore().collection("User_data").add({
        f_name: this.f_name,
        location: this.location,
        sex: this.sex,
        preferred_sex: this.preferred_sex,
        preferred_location: location,
        age: this.age,
        owner: this.UserId,
        email: this.email,
        create: firebase.firestore.FieldValue.serverTimestamp()
      }).then((docRef) => {
          this.toastCtrl.create({
            message: "User info has been added",
            duration: 2000
          }).then((toast) => {
            toast.present();
            this.navCtrl.navigateForward(['/profile']);
          })
      }).catch((err) => {
        this.toastCtrl.create({
          message: err.message,
          duration: 2000
        }).then((toast) => {
          toast.present();
          console.log(err);
      })
    })
    }).catch((err) => {
      this.toastCtrl.create({
        message: err.message,
        duration: 3000
      }).then((toast) => {
        toast.present();
        console.log(err);
      })
    })
  }

  gotoLogin(){
    this.navCtrl.back();
  }
}
