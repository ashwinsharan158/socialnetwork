import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyCw_cUiEj7a7Xb-sQ4M2z4IM4DOTxzZ61g",
  authDomain: "senseless-37e0c.firebaseapp.com",
  projectId: "senseless-37e0c",
  storageBucket: "senseless-37e0c.appspot.com",
  messagingSenderId: "338098339516",
  appId: "1:338098339516:web:2590b9c4e4fcf9465f9ab9",
  measurementId: "G-1K938LL05G"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
