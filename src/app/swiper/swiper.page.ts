import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.page.html',
  styleUrls: ['./swiper.page.scss'],
})
export class SwiperPage implements OnInit {

  constructor(private toastCtrl: ToastController, private navCtrl: NavController) {
   }

  ngOnInit() {
  }
  logout(){
      this.navCtrl.back(); }
}